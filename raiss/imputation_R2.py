"""
    Module for imputation performance evaluation

    Functions to generate test dataset with missing SNPs, impute missing SNPs
    and compare precision with original dataset.

    In other word, this module provides function to empirically measure imputation R2 on independant dataset.
"""

from raiss.pipes import save_chromosome_imputation
import multiprocessing
import itertools
from joblib import Parallel, delayed
import pandas as pd
import numpy as np



def generated_test_data(zscore, N_to_mask=5000, condition=None,  stratifying_vector=None, stratifying_bins=None):
    """
        Mask N_to_mask Snps in the dataframe zscore and return the dataframe with missing SNPs.
        Args:
            zscore (pandas DataFrame): Zscores DataFrame
            condition (None or pandas boolean Series): If None, SNPs will be mask randomly. If a pandas boolean series is passed, masked SNPs will be randomly chosen inside the SNPs which have True value.
            N_to_mask (int): Number of SNPs to mask.
    """
    try:
        if isinstance(condition, pd.Series)==True:
            print("Condition vector")
            masked = np.random.choice(zscore.index[condition], N_to_mask, replace=False)
        else:
            if isinstance(stratifying_vector, pd.Series)==True:
                print("Stratifying vector")
                inter_id = zscore.index.intersection(stratifying_vector.index).drop_duplicates(keep='first')
                stratifying_vector = stratifying_vector.loc[inter_id]
                masked = []
                binned = np.digitize(stratifying_vector, stratifying_bins)
                N_bins = len(stratifying_bins)-1
                print(N_bins)
                print(np.unique(binned))
                print(inter_id[(binned==(1))])
                print(N_to_mask // N_bins)

                for i in range(N_bins):
                    print(i)
                    print(np.where(binned==(i+1)))
                    masked = masked + list(np.random.choice(inter_id[(binned==(i+1))], N_to_mask // N_bins, replace=False))
                masked = np.array(masked)
                print(masked)
            else:
                masked = np.random.choice(zscore.index, N_to_mask, replace=False)
    except ValueError as ve:
        print("Couldn't sample {0} SNPs".format(N_to_mask))
        print("raise {0}".format(ve))

    known = zscore.index.difference(masked)
    print(known)
    print(zscore)
    return (zscore.loc[known], masked)

def imputation_performance(zscore_initial, zscore_imputed, masked):
    """
    Compute imputation performance on real data
    the performance is computed as the correlation between imputed and real values
    and as the mean absolute deviation between imputed and real values
    Args:
        zscore_initial (pandas DataFrame):
        zscore_imputed (pandas DataFrame):
        masked : SNPs ids which have been masked by imputation
    """
    try:
        N_masked = len(masked)
        masked = zscore_imputed.index.intersection(masked)
        N_imputed = len(masked)
        fraction_imputed = N_imputed / N_masked
        #cor = zscore_initial.loc[masked, "Z"].corr(zscore_imputed.loc[masked, "Z"].fillna(0))
        cor = zscore_initial.loc[masked, "Z"].corr(zscore_imputed.loc[masked, "Z"])

        error_abs= (zscore_initial.loc[masked, "Z"] - zscore_imputed.loc[masked, "Z"]).dropna().abs()
        MAE = error_abs.mean()
        error_quantile = error_abs.quantile([0,0.5,1])
        SNP_max_error = error_abs.idxmax()
        return {'N_SNP':len(masked),'fraction_imputed':fraction_imputed, 'cor':cor,
         'mean_absolute_error':MAE, 'median_absolute_error':error_quantile.loc[0.5],
         'min_absolute_error':error_quantile.loc[0.0],'max_absolute_error':error_quantile.loc[1.0], "SNP_max_error":SNP_max_error}

    except (ValueError,KeyError) as e:
        print(e) # If KeyError none of the masked_SNP are in the imputed dataframe
        res = np.nan
        return {'N_SNP':np.nan, 'fraction_imputed': np.nan, 'cor':np.nan, 'mean_absolute_error':np.nan, 'median_absolute_error':np.nan,
        'min_absolute_error':np.nan,'max_absolute_error':np.nan, "SNP_max_error":np.nan}

def z_amplitude_effect(zscore_folder, masked_folder, output_folder, ref_folder,
    ld_folder, gwas,ref_panel_preffix="",ref_panel_suffix=".eur.1pct.bim",
    z_treshold = [0, 1.0, 2.0, 3.0, 4.0, 5], window_size= 500000, buffer_size=125000,
     eigen_ratio = 0.1, chrom="chr22",  l2_regularization=0.1, R2_threshold=0.6, ratio_to_mask=0.5,
     n_cpu = np.max([1,multiprocessing.cpu_count()-2])):
    """
    Compute the imputation performance on SNPs with different amplitude
    The procedure is the following:
    * Masked Snps in the input dataset in function of the amplitude

    for eigen ratio in **eigen_ratio_grid**:
        * Impute SNPs
        * Compute performance on masked SNps
    Args:
        zscore_folder (str): path toward the input data folder
        masked_folder (str) : path toward the folder to save the dataframe with masked SNPs
        output_folder (str) : path toward the folder to save the imputed dataframe
        ref_folder (str) : path toward the folder to save the imputed dataframe
        ld_folder (str) : path toward the Linkage desiquilibrium matrices
         to save the imputed dataframe
        gwas (str): gwas identifier in the following format : 'CONSORTIA_TRAIT'
        chrom (str): chromosome in the format "chr.."
        z_treshold (list) : float list to select Z score to mask above Z score
        eigen_ratio (float): rcond parameter (must be between 0 and 1)
        window_size, buffer_size, l2_regularization, R2_threshold : imputation parameter (see raiss command line documentation)
        ratio_to_mask (int): the fraction of SNPs above the z_threshold to mask in the initial dataset to compute the correlation between true value and imputed value
        n_cpu (int): number of CPU to use for multithreading. Note that the default is the number of cpu on the computer running the program.
        This might fail on a HPC cluster where the number of cpu allocated to your job might be less than the number of CPU on the node running RAISS.
    """
    z_file = "{0}/z_{1}_{2}.txt".format(zscore_folder, gwas, chrom)
    zscore = pd.read_csv(z_file, index_col=0, sep="\t")
    zscore.drop_duplicates(keep='first', inplace=True)
    z_output = "{0}/z_{1}_{2}.txt".format(output_folder, gwas, chrom)
    dat_orig = pd.read_csv(z_file, sep="\t", index_col=0)
    dat_orig.drop_duplicates(keep='first', inplace=True)

    def run_imputation(z, i):
        print("Z score threshold : {}".format(z))
        condition = (dat_orig.Z.abs() > z)
        print("Number of SNPs verifying condition : {}".format(condition.sum()))
        N_to_mask = np.int(np.floor(condition.sum()*ratio_to_mask))

        res_masked = generated_test_data(dat_orig, N_to_mask= N_to_mask, condition = condition)
        z_masked = res_masked[0]
        z_masked_file = "{0}/z_{1}_{2}_{3}.txt".format(masked_folder, gwas, i, chrom)
        z_masked.to_csv(z_masked_file, sep="\t")

        masked_SNP = res_masked[1]
        tag = "_{}".format(z)
        gwas_proc = "{0}_{1}".format(gwas, i)
        save_chromosome_imputation(gwas_proc, chrom, window_size, buffer_size, l2_regularization, eigen_ratio, masked_folder, ref_folder, ref_panel_preffix, ref_panel_suffix, ld_folder, output_folder, R2_threshold, tag)

        z_output = "{0}/z_{1}_{2}{3}.txt".format(output_folder, gwas_proc, chrom, tag)
        dat_imp = pd.read_csv(z_output, sep="\t", index_col=0)
        R2 = imputation_performance(dat_orig, dat_imp, masked_SNP)
        print("R2 for {0} Zscore z_treshold: {1}".format(z, R2))
        return(R2)


    R2_val = Parallel(n_jobs=n_cpu)(delayed(run_imputation)(z, i) for i,z in enumerate(z_treshold))
    R2_serie =pd.DataFrame(R2_val, columns = ['N_SNP','fraction_imputed','cor', 'mean_absolute_error'], index = z_treshold)
    return(R2_serie)


def grid_search(zscore_folder, masked_folder, output_folder,
                ref_folder, ld_folder, gwas, chrom="chr22",
                eigen_ratio_grid = [0.5, 0.1, 0.01],
                ld_threshold_grid = [0,4, 10,20],
                window_size= 500000,
                 buffer_size=125000, l2_regularization=0.1, R2_threshold=0.6,
                 N_to_mask=5000,ref_panel_preffix="",ref_panel_suffix=".eur.1pct.bim", ld_type="plink",
                 stratifying_vector=None, stratifying_bins=None,
                 n_cpu = np.max([1,multiprocessing.cpu_count()-2])):
    """
    Compute the imputation performance for several eigen ratioself.
    The procedure is the following:
    * Masked N_to_mask Snps in the input dataset (chosen at random)
    * for eigen ratio in **eigen_ratio_grid**:
        * Impute SNPs
        * Compute performance on masked SNPs (R-square and mean absolute error)
    Args:
        zscore_folder (str): path toward the input data folder
        masked_folder (str) : path toward the folder to save the dataframe with masked SNPs
        output_folder (str) : path toward the folder to save the imputed dataframe
        ref_folder (str) : path toward the folder to save the imputed dataframe
        ld_folder (str) : path toward the Linkage desiquilibrium matrices
         to save the imputed dataframe
        gwas (str): gwas identifier in the following format : 'CONSORTIA_TRAIT'
        chrom (str): chromosome in the format "chr.."
        eigen_ratio_grid (list): list of eigen_ratio to test (must be between 0 and 1)
        ld_threshold_grid (list) : list of minimum-ld to test (must be > 0 )
        window_size, buffer_size, l2_regularization, R2_threshold : imputation parameter (see raiss command line documentation)
        N_to_mask (int): Number of SNPs masked in the initial dataset to compute the correlation between true value and imputed value
        ref_panel_suffix (str): suffix
        ld_type (str): The type of file where the LD is stored should be 'plink' or 'scipy'
        stratifying_vector (pd.Series) : a continuous vector containin one value per SNPs used to stratify the sampling of SNPs to mask
        stratifying_bins (list) : a vector specifying the boundary values to form the bins
    """
    z_file = "{0}/z_{1}_{2}.txt".format(zscore_folder, gwas, chrom)

    z_output = "{0}/z_{1}_{2}.txt".format(output_folder, gwas, chrom)
    dat_orig = pd.read_csv(z_file, sep="\t", index_col=0)

    res_masked = generated_test_data(dat_orig, N_to_mask, stratifying_vector=stratifying_vector, stratifying_bins=stratifying_bins)
    z_masked = res_masked[0]
    z_masked_file = "{0}/z_{1}_{2}.txt".format(masked_folder, gwas, chrom)
    z_masked.to_csv(z_masked_file, sep="\t")
    masked_SNP = res_masked[1]

    def run_imputation(param):
        cond = param[0]
        min_ld = param[1]
        tag = "_{0}_{1}".format(cond, min_ld)
        save_chromosome_imputation(gwas, chrom, window_size, buffer_size,
                                   l2_regularization, cond, masked_folder,
                                   ref_folder, ld_folder, output_folder,
                                   R2_threshold, tag, ref_panel_preffix,
                                   ref_panel_suffix, ld_type, minimum_ld = min_ld)

    param_grid = itertools.product(eigen_ratio_grid, ld_threshold_grid)

    R2_serie = pd.DataFrame({'N_SNP':np.nan, 'fraction_imputed': np.nan, 'cor':np.nan, 'mean_absolute_error':np.nan, 'median_absolute_error':np.nan,
    'min_absolute_error':np.nan,'max_absolute_error':np.nan, "SNP_max_error":np.nan}, index = pd.MultiIndex.from_tuples(param_grid, names=["eigen_ratio", "min_ld"]))
    param_grid = itertools.product(eigen_ratio_grid, ld_threshold_grid)
    Parallel(n_jobs=n_cpu)(delayed(run_imputation)(param) for param in param_grid)


    for min_ld in ld_threshold_grid:
        for rd in eigen_ratio_grid:
            z_output = "{0}/z_{1}_{2}_{3}_{4}.txt".format(output_folder, gwas, chrom, rd, min_ld)
            dat_imp = pd.read_csv(z_output, sep="\t", index_col=0)
            print(rd)
            try:
                res = imputation_performance(dat_orig, dat_imp, masked_SNP)
            except KeyError: # If KeyError none of the masked_SNP are in the imputed dataframe
                print(e)
                res = np.nan
            ind_loop = (rd, min_ld)
            R2_serie.loc[ind_loop] =res

            print(len(masked_SNP))
            print("Result for ind_loop {0} = cor: {1}, fraction_imputed: {2}".format(ind_loop, res["cor"], res["fraction_imputed"]))
    return(R2_serie)
