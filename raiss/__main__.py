import argparse
import pandas as pd
import sys
from raiss.filter_format_output import filter_output
from raiss.imputation_launcher import ImputationLauncher
from raiss.pipes import save_chromosome_imputation
from raiss.sanity_log import raiss_report
from raiss.imputation_R2 import grid_search


def launch_chromosome_imputation(args):
    """
    Function allow the calling of the ImputationLauncher.chromosome_imputation
    method from an entry point

    Args:
        args (dict): Argument parsed from the command line see the
        __main__.add_chromosome_imputation_argument(parser) function.
    """
    if None in [args.gwas, args.chrom,  args.zscore_folder, args.ref_folder, args.ld_folder, args.output_folder]:
        raise NameError("All --chrom, --gwas,--ref-folder, --ld-folder, --zscore-folder, --output-folder arguments are required")
    else:
        save_chromosome_imputation(args.gwas, args.chrom, args.window_size,
                             args.buffer_size, args.l2_regularization, args.eigen_threshold,
                             args.zscore_folder, args.ref_folder, args.ld_folder, args.output_folder,
                             args.R2_threshold, ref_panel_prefix=args.ref_panel_prefix,
                             ref_panel_suffix=args.ref_panel_suffix,
                             ld_type=args.ld_type, minimum_ld=args.minimum_ld)


def launch_sanity_checks(args):
    """
    Function allow the calling of the sanity_log.raiss_report from an entry point
    Args:
        args (dict): Argument parsed from the command line see the
        __main__.add_chromosome_imputation_argument(parser) function.
    """
    raiss_report(args.trait, args.harmonized_folder, args.imputed_folder, args.output_path, eval(args.chr_list))


def launch_performance_report(args):
    """
    Function allow the calling of the sanity_log.raiss_report from an entry point
    Args:
        args (dict): Argument parsed from the command line see the
        __main__.add_chromosome_imputation_argument(parser) function.
    """
    if args.n_cpu=="unspecified":
        print("the number of cpu is not unspecified will be infered")
        performance_grid = grid_search(args.harmonized_folder,
                    args.masked_folder,args.imputed_folder,
                    args.ref_folder,args.ld_folder,
                    args.gwas, chrom=args.chrom,
                    eigen_ratio_grid = eval(args.eigen_ratio_grid),
                    ld_threshold_grid = eval(args.ld_threshold_grid),
                    window_size= int(args.window_size),
                    buffer_size=int(args.buffer_size),
                    l2_regularization=float(args.l2_regularization),
                    R2_threshold=float(args.R2_threshold),
                    N_to_mask=int(args.N_to_mask),
                    ref_panel_preffix=args.ref_panel_prefix,
                    ref_panel_suffix=args.ref_panel_suffix,
                    ld_type=args.ld_type)
    else:
        performance_grid = grid_search(args.harmonized_folder,
                    args.masked_folder,args.imputed_folder,
                    args.ref_folder,args.ld_folder,
                    args.gwas, chrom=args.chrom,
                    eigen_ratio_grid = eval(args.eigen_ratio_grid),
                    ld_threshold_grid = eval(args.ld_threshold_grid),
                    window_size= int(args.window_size),
                    buffer_size=int(args.buffer_size),
                    l2_regularization=float(args.l2_regularization),
                    R2_threshold=float(args.R2_threshold),
                    N_to_mask=int(args.N_to_mask),
                    ref_panel_preffix=args.ref_panel_prefix,
                    ref_panel_suffix=args.ref_panel_suffix,
                    ld_type=args.ld_type, n_cpu=int(args.n_cpu))
    print("PERFORMANCE REPORT:")
    print(performance_grid)
    output_file = "{0}/perf_report_{1}.csv".format(args.output_path,args.gwas)
    performance_grid.to_csv(output_file)
    print("Saved at {}".format(output_file))


def add_chromosome_imputation_argument():
    parser = argparse.ArgumentParser(description="raiss command launch imputation for one trait on one chromosome")
    subparser = parser.add_subparsers(help="available suboptions : sanity-check")
    #subparser.required = True
    #RAISS by chr
    #raiss = subparser.add_parser("")
    parser.add_argument('--chrom', help= "chromosome to impute to the chr\d+ format")
    parser.add_argument('--gwas', help= "GWAS to impute to the consortia_trait format")

    parser.add_argument('--ref-folder', help= "reference panel location (used to determine which snp to impute)")
    parser.add_argument('--ld-folder', help= "Location  LD correlation matrices")
    parser.add_argument('--zscore-folder', help= "Location of the zscore files of the gwases to impute")
    parser.add_argument('--output-folder', help= "Location of the impute zscore files")

    parser.add_argument('--window-size', help= "Size of the non overlapping window", default = 500000)
    parser.add_argument('--buffer-size', help= "Size of the buffer around the imputation window", default = 125000)
    parser.add_argument('--l2-regularization', help= "Size of the small value added to the diagonal of the covariance matrix before inversion", default = 0.1)
    parser.add_argument('--eigen-threshold', help= "threshold under which eigen vectors are removed for the computation of the pseudo inverse", default = 0.1)

    parser.add_argument('--R2-threshold', help= "R square (imputation quality) threshold bellow which SNPs are filtered from the output", default = 0.6)
    parser.add_argument("--ld-type", help= "'Two options are available : 'plink' or 'scipy'. Ld can be supplied as plink command --ld-snp-list output files (see raiss.ld_matrix.launch_plink_ld to compute these data using plink) or as a couple of a scipy sparse matrix (.npz )and an .csv containing SNPs index", default="plink")
    parser.add_argument('--ref-panel-prefix', help= "prefix for the reference panel files", default = "")
    parser.add_argument('--ref-panel-suffix', help= "suffix for the reference panel files", default = ".bim")
    parser.add_argument('--minimum-ld', help = "this parameter ensure that their is enough typed SNPs around the imputed loci to perform a high accuracy imputation", default = 4)

    parser.set_defaults(func=launch_chromosome_imputation)

    # Add sub options
    # --------------------Suboption sanity check ----------------------------#
    sanity_check = subparser.add_parser("sanity-check", help="count imputed SNPs and significant hit before and after imputation")
    sanity_check.add_argument("--trait", required=True, help="trait on which to run the sanity check")
    sanity_check.add_argument("--harmonized-folder", required=True, help="folder containing data before imputation")
    sanity_check.add_argument("--imputed-folder", required=True, help="folder containing imputed files")
    sanity_check.add_argument("--output-path", required=False, default="./raiss_report", help="path+prefix of the report file, the file will be suffixed by the trait name and .txt")
    sanity_check.add_argument("--chr-list", required=False, default="range(1,23)")
    sanity_check.set_defaults(func=launch_sanity_checks)

    # --------------------Suboption raiss performance-report ----------------------------#
    performance_grid = subparser.add_parser("performance-grid-search", help="assess RAISS performances for a range of parameter by masking SNPs, re-imputing them and comparing imputed value to initial ones")
    performance_grid.add_argument("--harmonized-folder", required=True, help="folder containing data before imputation")
    performance_grid.add_argument("--masked-folder", required=True, help="folder to store zscore with masked SNPs (raiss performance-grid-search will generate them)")
    performance_grid.add_argument("--imputed-folder", required=True, help="folder to store imputed files")
    performance_grid.add_argument("--output-path", required=True, default="./raiss_report", help="path+prefix of the report file, the file will be suffixed by the trait name and .txt")
    performance_grid.add_argument("--eigen-ratio-grid", required=True, default='[0.5, 0.1, 0.01]', help="main parameter to tune. Adjust the regularization when computing the pseudo inverse of LD matrices")
    performance_grid.add_argument("--ld-threshold-grid", required=False, default='[0,4, 10,20]', help="optional parameter to tune. raising the threshold will augment the number neighboring of SNPs required for the imputation to be consider valid (i.e. missing SNPs with only few neighbor won't be imputed). Empirically this parameter seems to impact more the maximum error than the mean/median error")
    performance_grid.add_argument("--N-to-mask", required=False, default=5000, help="Number of SNPs to mask. Should only be a relatively small fraction (e.g. ~1/10)of the number of your initial data")
    performance_grid.add_argument("--n-cpu", required=False, default='unspecified', help="Number of cpu to use to run imputation in Parallel for each parameter value")
    performance_grid.set_defaults(func=launch_performance_report)

    return(parser)


def main():
    print("", file=sys.stderr)
    print("  *******       *******    *******   *******   *******", file=sys.stderr)
    print("  **     **    **     **     ***    **        **", file=sys.stderr)
    print("  **     **   **       **    ***    **        **", file=sys.stderr)
    print("  ** *****    **       **    ***      ******    ******", file=sys.stderr)
    print("  **     **   ***********    ***           **        **", file=sys.stderr)
    print("  **     **   **       **    ***           **        **", file=sys.stderr)
    print("  **     **   **       **  *******   *******   *******", file=sys.stderr)
    print("", file=sys.stderr)
    print("", file=sys.stderr)
    parser = add_chromosome_imputation_argument()
    args = parser.parse_args()
    args.func(args)


if __name__=="__main__":

    main()
