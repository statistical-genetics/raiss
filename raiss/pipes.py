"""
    module to manage the creation of files to save the results of imputation
"""

import argparse
import pandas as pd
import numpy as np
from raiss.filter_format_output import filter_output
from raiss.imputation_launcher import ImputationLauncher

def save_chromosome_imputation(gwas, chrom, window_size, buffer_size,
                               l2_regularization, eigen_threshold, zscore_folder,
                            ref_folder, ld_folder, output_folder, R2_threshold,
                             tag="", ref_panel_prefix="", ref_panel_suffix=".eur.1pct.bim", ld_type="plink", minimum_ld=4):
    """
    module to manage the creation of files to save the results of imputation
    Args:
        gwas (str): gwas identifier in the following format : 'CONSORTIA_TRAIT'
        chrom (str): chromosome in the format "chr.."
        zscore_folder (str): path toward the input data folder
        masked_folder (str) : path toward the folder to save the dataframe with masked SNPs
        output_folder (str) : path toward the folder to save the imputed dataframe
        ref_folder (str) : path toward the folder to save the imputed dataframe
        ld_folder (str) : path toward the Linkage desiquilibrium matrices
         to save the imputed dataframe
        eigen_threshold (float): list of eigen_threshold to test (must be between 0 and 1)
        window_size, buffer_size, l2_regularization, R2_threshold : imputation parameter (see raiss command line documentation)
    """

    print("Imputation of {0} gwas for chromosome {1}".format(gwas, chrom))
    # Imputer settings
    imputer = ImputationLauncher( window_size=int(window_size), buf=int(buffer_size), lamb= float(l2_regularization), pinv_rtol = float(eigen_threshold), ld_type = ld_type)
    # Reading of inputs
    z_file = "{0}/z_{1}_{2}.txt".format(zscore_folder, gwas, chrom)
    zscore = pd.read_csv(z_file, index_col=0, sep="\t")
    ref_panel_file = ref_folder + "/"+ ref_panel_prefix + chrom + ref_panel_suffix
    ref_panel = pd.read_csv(ref_panel_file, sep="\t", names=['chr', "nothing", 'pos', 'Ref_all', 'alt_all'], index_col = 1)

        
     # imputation
    imputed_zscore = imputer.chromosome_imputation(chrom, zscore, ref_panel, ld_folder)
    print("Imputation DONE")
    imputed_zscore = filter_output(imputed_zscore, float(R2_threshold), minimum_ld=float(minimum_ld))

    imputed_zscore["imputed"] = imputed_zscore.Var>-0.5

    variant_not_in_panel = zscore.index.difference(ref_panel.index)
    if len(variant_not_in_panel) >0:
        zscore_variant_not_in_panel = zscore.loc[variant_not_in_panel].copy()
        zscore_variant_not_in_panel = zscore_variant_not_in_panel[["rsID","pos","A0","A1","Z"]]
        zscore_variant_not_in_panel["Var"] = -1.0
        zscore_variant_not_in_panel["ld_score"] = np.inf
        zscore_variant_not_in_panel["imputation_R2"] = 2.0
        zscore_variant_not_in_panel["imputed"] = False

        imputed_zscore = pd.concat([imputed_zscore, zscore_variant_not_in_panel])

    # Formatting and filtering
    # and Saving results
    z_fo = "{0}/z_{1}_{2}{3}.txt".format(output_folder, gwas, chrom, tag)
    imputed_zscore.to_csv(z_fo, sep="\t", index=False)
    print("Save imputation done at {0}".format(z_fo))
