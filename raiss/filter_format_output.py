"""
    Module to filter SNPs on imputation quality
    and format output for JASS
"""


def filter_output(zscores, R2_threshold = 0.6, minimum_ld = 5):
    """
    procedure that format output for JASS

    Args:
        zscores (pandas dataframe): imputed zscore
         (as outputed from imputation_launcher.chromosome_imputation)
        fout (filename): filename where to save the formatted and filtered output
        R2_threshold (float): R square threshold bellow which SNPs are filtered from the output
    """


    zscores.reset_index(inplace = True)
    zscores = zscores[['index', 'pos', 'A0', 'A1', 'Z', 'Var', "ld_score"]].copy()
    zscores["imputation_R2"] = 1-zscores["Var"]
    zscores.columns = ['rsID','pos','A0','A1','Z', 'Var', "ld_score", "imputation_R2"]

    NSNPs_bf_filt = zscores.shape[0]
    NSNPs_initial = (zscores.imputation_R2==2.0).sum()
    NSNPs_imputed = (zscores.imputation_R2!=2.0).sum()
    NSNPs_ld_filt = (zscores.ld_score < float(minimum_ld)).sum()
    NSNPs_R2_filt = (zscores.imputation_R2 < R2_threshold).sum()
    zscores = zscores.loc[(zscores.imputation_R2 > R2_threshold) & (zscores.ld_score > float(minimum_ld))]
    NSNPs_af_filt = zscores.shape[0]

    print("IMPUTATION REPORT")
    print("Number of SNPs:")
    print("before filter: {}".format(NSNPs_bf_filt))
    print("not imputed: {}".format(NSNPs_initial))
    print("imputed: {}".format(NSNPs_imputed))
    print("filtered because of ld: {}".format(NSNPs_ld_filt))
    print("filtered because of R2: {}".format(NSNPs_R2_filt))
    print("before filter: {}".format(NSNPs_af_filt))

    return zscores

