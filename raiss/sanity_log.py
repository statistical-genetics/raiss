"""
    Module to generate logs on imputation results
    Notably, perform the following tasks :
    - Report the total number of SNPs before and after imputation
    - Report the number of significant SNPs before and after imputation for
    several threshold
    - Report the number of SNPs by imputation quality

"""

import pandas as pd
from scipy.stats import norm


def count_snps(trait, harmonized_folder, imputed_folder, chr_list):
    pval_limit=[1,0.05, 5*10**-6, 5*10**-8, 0]
    nsf = norm.isf(pval_limit)
    nsnp_total_harmonized = 0
    nsnp_total_imputed = 0

    count_by_signif_harm = []
    count_by_signif_imp = []
    count_by_imp_quality = []

    count_by_reg_signif_harm = []
    count_by_reg_signif_imp = []

    for chr in chr_list:
        hf = pd.read_csv("{0}/{1}_chr{2}.txt".format(harmonized_folder, trait, chr), sep="\t")
        imf = pd.read_csv("{0}/{1}_chr{2}.txt".format(imputed_folder, trait, chr), sep="\t")

        nsnp_total_harmonized += hf.shape[0]
        nsnp_total_imputed += imf.shape[0]

        count_by_signif_harm.append(pd.cut(hf.Z.abs(), nsf).value_counts())
        count_by_signif_imp.append(pd.cut(imf.Z.abs(), nsf).value_counts())
        count_by_imp_quality.append(pd.cut(imf.loc[imf.Var> -1, "imputation_R2"], [0,0.6,0.8,0.9,0.95,1]).value_counts())

        hf["Mb"] =(hf.pos / 10**6).astype(int).astype(str)
        hf['Z'] = hf.Z.abs()

        imf["Mb"] =(imf.pos / 10**6).astype(int).astype(str)
        imf['Z'] = imf.Z.abs()

        count_by_reg_signif_harm.append(pd.cut(hf.groupby("Mb").Z.max(), nsf).value_counts())
        count_by_reg_signif_imp.append(pd.cut(imf.groupby("Mb").Z.max(), nsf).value_counts())

    signif_harm = pd.concat(count_by_signif_harm, axis=1).sum(1)
    signif_harm_reg = pd.concat(count_by_reg_signif_harm, axis=1).sum(1)

    signif_imp = pd.concat(count_by_signif_imp, axis=1).sum(1)
    signif_imp_reg = pd.concat(count_by_reg_signif_imp, axis=1).sum(1)

    R2_quality = pd.concat(count_by_imp_quality, axis=1).sum(1)

    signif = pd.concat([signif_harm, signif_harm_reg, signif_imp, signif_imp_reg],keys=["harmonized","harmonized_by_1Mbregion","imputed", "imputed_by_1MBregion"], axis=1)
    signif["imputed_proportion"] = 1 - (signif["harmonized"]/  signif['imputed'])
    signif["imputed_hit_OR"] = (signif["imputed_by_1MBregion"]/  (signif['imputed_by_1MBregion'] + signif["harmonized_by_1Mbregion"]))/ (signif["imputed_by_1MBregion"].sum()/  (signif['imputed_by_1MBregion'] + signif["harmonized_by_1Mbregion"]).sum())
    prop = (1 - (nsnp_total_harmonized / nsnp_total_imputed))
    signif.index = ["{:.2e}-{:.2e}".format(pval_limit[i+1], pval_limit[i]) for i in range(4)]

    return({'proportion':prop, "total_harmonized":nsnp_total_harmonized,
        "total_imputed":nsnp_total_imputed, "signif_count":signif, "R2_quality":R2_quality})

def report_snps(res_dict, trait, filename="raiss_report"):
    output_file="{}_{}.txt".format(filename, trait)
    with open(output_file, "w") as f:
        f.write("Number of SNPs\n")
        f.write("in harmonized file: {}\n".format(res_dict["total_harmonized"]))
        f.write("in imputed file: {}\n".format(res_dict["total_imputed"]))
        f.write("Proportion imputed : {}\n".format(res_dict['proportion']))
        f.write("\n")
        f.write("\n Number of SNPs by level of significance\n")
        f.write(res_dict['signif_count'].to_string())
        f.write("\n".format(res_dict))
        f.write("\n Number of imputed SNPs by level of imputation quality\n")
        f.write(res_dict['R2_quality'].to_string())
    f.close()

def report_snps_csv(res_dict, trait, filename="raiss_report"):
    # I save the log in three .csv files.
    df = pd.DataFrame({'Number of SNPs in harmonized file': [res_dict["total_harmonized"]],
                       'Number of SNPs in imputed file': [res_dict["total_imputed"]],
                       'Proportion imputed': [res_dict['proportion']]})
    df.to_csv("{}_{}_Number_of_SNPs.csv".format(filename, trait), index=False)

    # Number of SNPs by level of significance
    res_dict['signif_count'].to_csv("{}_{}_Number_of_SNPs_by_level_of_significance.csv".format(filename, trait))

    # Number of imputed SNPs by level of imputation quality
    res_dict['R2_quality'].to_csv = "{}_{}_Number_of_SNPs_by_level_of_imputation_quality.csv".format(filename, trait)

def raiss_report(trait, harmonized_folder, imputed_folder, filename="raiss_report", chr_list=range(1,23)):
    """
    Function to compute a report for one trait
    """
    res_dict = count_snps(trait, harmonized_folder, imputed_folder, chr_list)
    report_snps_csv(res_dict, trait, filename)
