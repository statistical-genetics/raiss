# -*- coding: utf-8 -*-
"""Imputation launcher

Function set to launch SNP imputation on a complete chromosome or
on the genome

"""

import glob
import pandas as pd
from raiss.windows import prepare_zscore_for_imputation, impg_like_imputation, realigned_zfiles_on_panel

class ImputationLauncher(object):
    """
    Class to perform imputation of snp from summary statistic
    """
    def __init__(self, window_size=10000, buf=2500,
                 lamb= 0.01, pinv_rtol = 0.01, ld_type="plink"):
        """
        Initialise the imputation object. Fix the windows size, the buffer size
        and the kind of imputation employed

        Args:
            window_size (int): size of the imputation window in bp
            buffer (int): the size of the padding around the windows of
                imputation (relevant only for batch imputation)
            lamb (float): size of the increment added to snp correlation
                matrices to make it less singular
            pinv_rtol (float): the rtol scipy.linalg.pinv function argument.
                The scipy.linalg.pinv is used to invert the correlation matrices
        """

        self.window_size = window_size
        self.buffer = buf
        self.lamb = lamb
        self.rtol = pinv_rtol
        self.ld_type = ld_type

    def chromosome_imputation(self, chrom, zscore, ref_panel, ld_folder):
        """
        Impute the panel zscore score for one chromosome and with the specified
        parameters

        Args:
            chrom (str): chromosome "chr*"
            zscore (pandas dataframe): known zscore
            ref_panel (DataFrame): chromosome reference panel dataframe
            ld_folder (str): path of the folder containing linkage desiquilibrium matrices

        Returns:
            pandas dataframe: Imputed zscore dataframe
        """

        def get_file_name(x):
            return x.split("/")[-1].split(".")[0]

        pattern = "{0}/{1}_*".format(ld_folder, chrom)


        zscore = prepare_zscore_for_imputation(ref_panel, zscore)
        zscore_results = zscore.copy(deep=True)

        def imputer(ld_file):
            return impg_like_imputation(ld_file, ref_panel, zscore,
                                        self.window_size, self.buffer,
                                         self.lamb, self.rtol, self.ld_type)

        ld_file_list = set(map(get_file_name, glob.glob(pattern)))
        print(ld_file_list)
        if len(ld_file_list) == 0:
            raise FileNotFoundError("No LD matrices were found at {0} using {1} pattern. Is the path to LD matrices ({0}) correct".format(ld_folder, pattern))

        for ld_file in ld_file_list:
            print("processing Region: {0}".format(ld_file))
            ld_path = "{0}/{1}".format(ld_folder, ld_file)
            ld_batch = imputer(ld_path)
            zscore_results = pd.concat([zscore_results, ld_batch])
        zscore_results = realigned_zfiles_on_panel(ref_panel, zscore_results)
        zscore_results.sort_values(by="pos", inplace=True)

        return zscore_results
