#!/bin/bash
#SBATCH --mem=32000
#SBATCH -o /pasteur/projets/policy01/PCMA/WKD_Hanna/impute_for_jass/genome_imputation_script/imputation_log/IMPFJ_%a.log
#SBATCH -e /pasteur/projets/policy01/PCMA/WKD_Hanna/impute_for_jass/genome_imputation_script/error_log/error_%a.log

#define data location
module load Python/3.6.0
output_folder="/pasteur/projets/policy01/PCMA/1._DATA/ImFJ_imputed_zfiles"
ref_folder="/pasteur/projets/policy01/PCMA/1._DATA/ImpG_refpanel"
zscore_folder="/pasteur/projets/policy01/PCMA/1._DATA/ImpG_zfiles"
ld_folder="/pasteur/projets/policy01/PCMA/WKD_Hanna/impute_for_jass/berisa_ld_block"


args=$(head -n ${SLURM_ARRAY_TASK_ID} $1 | tail -n 1)
study=$(echo $args | cut -d' ' -f1)
chrom=$(echo $args | cut -d' ' -f2)
#quick fix to access entry point on tars
alias impute_jass="python3 /pasteur/homes/hjulienn/.local/lib/python3.6/site-packages/impute_jass/__main__.py"
#study="GIANT_HEIGHT"
#chrom="chr22"

echo "Study: ${study}; Chrom: ${chrom}"

#time impute_jass --chrom ${chrom} --gwas ${study} --ref-folder ${ref_folder} --ld-folder ${ld_folder} --zscore-folder ${zscore_folder} --output-folder ${output_folder}
time python3 /pasteur/homes/hjulienn/.local/lib/python3.6/site-packages/impute_jass/__main__.py --chrom ${chrom} --gwas ${study} --ref-folder ${ref_folder} --ld-folder ${ld_folder} --zscore-folder ${zscore_folder} --output-folder ${output_folder}
