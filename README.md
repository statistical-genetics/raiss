RAISS (Robust and accurate imputation from summary statistics) is a python 
package enabling the imputation of SNP summary statistics from the neighboring 
SNPs by taking advantage of the Linkage desiquilibrium.

The package documentation is : http://statistical-genetics.pages.pasteur.fr/raiss/