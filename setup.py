from setuptools import setup, find_packages

setup(name='raiss',
      version='4.0.1',
      description='Imputation of summary statistics',
      url='https://gitlab.pasteur.fr/statistical-genetics/raiss',
      author='Hanna Julienne',
      author_email='hanna.julienne@pasteur.fr',
      install_requires = ['scipy', "pandas", "joblib"],
      license='MIT',
      #package_dir = {'': 'jass_preprocessing'},
      packages= ['raiss'],

      package_data = {'raiss':['./data/*.csv']},
      zip_safe=False,

      entry_points={
          'console_scripts' : [
            'raiss = raiss.__main__:main'
          ]
      }
      )
